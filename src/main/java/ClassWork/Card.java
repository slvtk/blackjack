package ClassWork;

import java.util.ArrayList;

public class Card {

    private Integer value;
    private String title;
    private String suit;

    public Card(Integer value, String title, String suit) {
        this.value = value;
        this.title = title;
        this.suit = suit;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSuit() {
        return suit;
    }

    public void setSuit(String suit) {
        this.suit = suit;
    }

    @Override
    public String toString() {
        return "\nВес карты: " + value + ", Карта: " + title + " "+ suit;
    }

    public static ArrayList<Card> getDeck(){
        Integer[] values=new Integer[]{2,3,4,6,7,8,9,10,11};
        String[] titles=new String[]{"Jack","Queen","King","6","7","8","9","10","Ace"};
        String[] suits=new String[]{"diamonds", "hearts", "spades","clubs"};
        ArrayList<Card> deck=new ArrayList<>();

        for(int i=0;i<values.length;i++){
            for (String suit : suits) {
                deck.add(new Card(values[i], titles[i], suit));
            }
        }
        return deck;
    }

    public static String getURL(Card card){
        return "/img/" +card.getTitle()+"_"+card.getSuit()+".png";
    }
}
