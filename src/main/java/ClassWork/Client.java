package ClassWork;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) throws IOException {
        final String HOST = "localhost";
        final int PORT = 1234;
        System.out.println("Connecting to " + HOST + ":" + PORT + ".");
        Socket s = new Socket(HOST, PORT);
        System.out.println("Connected");

        BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
        PrintWriter out = new PrintWriter(s.getOutputStream(), true);
        Scanner scan = new Scanner(System.in);
        System.out.println(in.readLine());
        User player = new User();
        Game blackjack = new Game();
        player.setName(scan.next());
        Integer sum = blackjack.playGame(player);
        out.println(sum);
    }
}
