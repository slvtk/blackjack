package ClassWork;

import java.util.List;
import java.util.Random;

public class User {
    private String name;
    private Integer score;
    private Integer money;

    public User() {
        this.score = 0;
        this.money = 1000;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public void getCard(List<Card> deck, List<Card> pool){
        Card randomCard;
        randomCard=deck.get(new Random().nextInt(deck.size()));//Берем рандомную карту
        deck.remove(randomCard);
        pool.add(randomCard);
    }
}
