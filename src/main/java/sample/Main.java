package sample;

import ClassWork.Card;
import ClassWork.User;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class Main extends Application {

    private User user = new User();
    private AtomicInteger cardCoordinate = new AtomicInteger(10);

    @Override
    public void start(Stage primaryStage) throws Exception {

        //Основное окно
        AnchorPane root = FXMLLoader.load(getClass().getResource("/sample.fxml"));
        Pane result = (Pane) root.lookup("#result");
        TextField firstUserScore = (TextField) root.lookup("#firstUserScore");
        TextField secondUserScore = (TextField) root.lookup("#secondUserScore");

        result.setVisible(false);
        Pane pane = new Pane();
        root.getChildren().add(pane);
        primaryStage.setTitle("BlackJack");
        primaryStage.setScene(new Scene(root, 740, 391));
        primaryStage.setResizable(false);
        primaryStage.show();

        //Модальное окно
        TextField name = new TextField("Ваше имя: ");
        StackPane secondaryLayout = new StackPane();
        secondaryLayout.getChildren().add(name);
        Stage secondStage = new Stage();
        secondStage.setTitle("Please, enter your name");
        secondStage.setScene(new Scene(secondaryLayout, 189, 100));
        secondStage.setResizable(false);
        secondStage.initModality(Modality.WINDOW_MODAL);
        secondStage.initOwner(primaryStage);
        //System.out.println(primaryStage.getX());
        secondStage.setX(primaryStage.getX() + 276);
        secondStage.setY(primaryStage.getY() + 130);
        secondStage.show();
        name.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                user.setName(name.getText());
                //System.out.println(user.getMoney());
                secondStage.close();
            }
        });

        //Обьявление переменных
        Button stop = (Button) root.lookup("#stop");
        stop.setVisible(false);
        TextField bet = (TextField) root.lookup("#bet");
        //Поле денег игрока
        TextField money = (TextField) root.lookup("#money");
        money.setEditable(false);
        money.appendText(Integer.toString(user.getMoney()));
        List<Card> deck = Card.getDeck();
        Button more = (Button) root.lookup("#more");
        //Поле очков
        TextField score = (TextField) root.lookup("#score");
        score.setEditable(false);
        score.appendText("0");

        final String HOST = "localhost";
        final int PORT = 1234;
        System.out.println("Connecting to " + HOST + ":" + PORT + ".");
        Socket s = new Socket(HOST, PORT);
        System.out.println("Connected");

        BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
        PrintWriter out = new PrintWriter(s.getOutputStream(), true);
        //out.println("Hello");

        //Поле ставки
        bet.setOnAction(event -> {
            //Тут должна быть ВАЛИДАЦИЯ
        });


        //Кнопка остановки игры
        stop.setOnAction(event -> {
            stop.setVisible(false);
            more.setVisible(false);
            result.setVisible(true);
            firstUserScore.appendText(user.getScore().toString());
            firstUserScore.setEditable(false);
            out.println(Integer.parseInt(score.getText()));
            try {
                secondUserScore.appendText(in.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
            secondUserScore.setEditable(false);
            pane.getChildren().removeAll(pane.getChildren());
            cardCoordinate.set(10);
            more.setText("Играть");
            user.setScore(0);
            score.clear();
            score.appendText("0");

            //Логика игры
            int isWin = isWin(Integer.parseInt(firstUserScore.getText()), Integer.parseInt(secondUserScore.getText()));
            switch (isWin) {
                case 1:
                    user.setMoney(user.getMoney() + Integer.parseInt(bet.getText()) * 2);
                    break;
                case 0:
                    user.setMoney(user.getMoney() + Integer.parseInt(bet.getText()));
                    break;
                case -1:
                    break;
            }
            money.clear();
            money.appendText(user.getMoney().toString());
        });

        //Кнопка на панели предлагающая сыграть еще
        Button resultSubmit = (Button) root.lookup("#resultSubmit");
        resultSubmit.setOnAction(event -> {
            bet.setVisible(true);
            more.setVisible(true);
            firstUserScore.clear();
            secondUserScore.clear();
            result.setVisible(false);
        });


        //Набор карт
        more.setOnAction(event -> {
            stop.setVisible(true);
            bet.setVisible(false);
            more.setText("Еще карту");
            if (cardCoordinate.get() == 10) {
                user.setMoney(user.getMoney() - Integer.parseInt(bet.getText()));
                money.clear();
                money.appendText(user.getMoney().toString());
                for (int i = 0; i < 2; i++) {
                    user.setScore(user.getScore() + getCard(pane, deck).getValue());
                    score.clear();
                    score.appendText(user.getScore().toString());
                }
            } else if (cardCoordinate.get() <= 660) {
                user.setScore(user.getScore() + getCard(pane, deck).getValue());
                score.clear();
                score.appendText(user.getScore().toString());
            }
        });
    }

    private int isWin(int first, int second) {
        int result = -2;
        if (first <= 21 && (second > 21 || second < first)) {
            result = 1;
        } else if (((second <= 21) && (first == second)) || ((first > 21) && (second > 21))) {
            result = 0;
        } else if (first > 21 || first < second) {
            result = -1;
        }
        return result;
    }


    private Card getCard(Pane pane, List<Card> deck) {
        Card randomCard = deck.get(new Random().nextInt(deck.size()));
        ImageView card = new ImageView(new Image(Card.getURL(randomCard)));
        KeyValue xValue;
        KeyValue yValue=new KeyValue(card.yProperty(),100);

        card.setFitWidth(120);
        card.setFitHeight(180);
        xValue=new KeyValue(card.xProperty(),cardCoordinate.get());
        //card.setX(cardCoordinate.get());
        //card.setY(100);
        ScaleTransition st=new ScaleTransition(Duration.millis(100), card);
        ScaleTransition st1=new ScaleTransition(Duration.millis(100), card);
        st.setFromX(-1);
        st.setToX(0);
        st.play();
        st.setOnFinished(e->{
            st1.setFromX(0);
            st1.setToX(1);
            st1.play();
        });
        KeyFrame keyFrame=new KeyFrame(Duration.millis(100),xValue,yValue);
        Timeline timeline=new Timeline();
        timeline.getKeyFrames().addAll(keyFrame);
        timeline.play();
        pane.getChildren().add(card);
        cardCoordinate.set(cardCoordinate.get() + 60);
        return randomCard;
    }


    public static void main(String[] args) {
        launch(args);
    }
}