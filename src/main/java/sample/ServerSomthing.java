package sample;

import java.io.*;
import java.net.Socket;

class ServerSomthing extends Thread {

    private Socket socket; // сокет, через который сервер общается с клиентом,
    // кроме него - клиент и сервер никак не связаны
    private BufferedReader in; // поток чтения из сокета
    private PrintWriter out; // поток записи в сокет

    public ServerSomthing(Socket socket) throws IOException {
        this.socket = socket;
        // если потоку ввода/вывода приведут к генерированию исключения, оно проброситься дальше
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);
        start(); // вызываем run()
    }

    @Override
    public void run() {
        String score;
        try {
            while (true) {
                score = in.readLine();

                System.out.println(score);
                for (ServerSomthing vr : Server.serverList) {
                    if (vr != this) {
                        vr.send(score);
                    }
                }
            }
        } catch (IOException ignored) {
        }
    }

    private void send(String msg) {
        out.println(msg);
    }
}
